/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.io.InputStream;
import java.sql.Connection;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.connect;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;
import static org.omg.CORBA.AnySeqHelper.insert;

/**
 *
 * @author IDEAPAD GAMING
 */
public class application2 extends javax.swing.JFrame {

    /**
     * Creates new form application2
     */
    public application2(String nama, String username, String asal) {
        initComponents();
        tbkirimduri();
        tbkirimperawang();
        this.setLocationRelativeTo(null);
        jumlah_kirimperawang();
        jumlah_kirimduri();
        int jmlall = (Integer.parseInt(jmlduri.getText())) + (Integer.parseInt(jmlperawang.getText()));
        String jml = String.valueOf(jmlall);
        jumlah_kirimall.setText(jml);
        user.setText(username);
        kota.setText(asal);
        nama_t.setText(nama);
        masukdata();
    }

    public void jumlah_kirimperawang() {
        try {
            String sql1 = "SELECT COUNT(*) FROM tbkirim_perawang ";
            java.sql.Connection con = (Connection) connect.configDB();
            java.sql.PreparedStatement sts = con.prepareStatement(sql1);
            java.sql.ResultSet data = sts.executeQuery();
            if (data.next()) {
                String data1 = data.getString("COUNT(*)");
                jmlduri.setText(data1);
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, "error" + e.getMessage());
        }
    }

    public void jumlah_kirimduri() {
        try {
            String sql1 = "SELECT COUNT(*) FROM tbkirim_duri ";
            java.sql.Connection con = (Connection) connect.configDB();
            java.sql.PreparedStatement sts = con.prepareStatement(sql1);
            java.sql.ResultSet data = sts.executeQuery();
            if (data.next()) {
                String data1 = data.getString("COUNT(*)");
                jmlperawang.setText(data1);
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, "error" + e.getMessage());
        }
    }

    public void masukdata() {
        String asal = null;

        if (insert.getSelectedItem() == "Duri") {
            asal = "duri";
            System.out.println(asal);
            new insert_barang(user.getText(), nama_t.getText(), kota.getText(), asal).setVisible(true);
        } else if (insert.getSelectedItem() == "Perawang") {
            asal = "perawang";
            new insert_barang(user.getText(), nama_t.getText(), kota.getText(), asal).setVisible(true);
        }
    }

    public void tbkirimduri() {
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("no");
        model.addColumn("Id Barang");
        model.addColumn("nama");
        model.addColumn("pengirim");
        model.addColumn("alamat");
        model.addColumn("tujuan");
        model.addColumn("berat");
        model.addColumn("biaya");
        model.addColumn("status");
        model.addColumn("tgl krim");
        model.addColumn("tgl estimasi");

        try {
            String sql = "SELECT * FROM tbkirim_duri";
            java.sql.Connection con = (Connection) connect.configDB();
            java.sql.Statement sts = con.createStatement();
            java.sql.ResultSet data = sts.executeQuery(sql);
            while (data.next()) {
                model.addRow(new Object[]{
                    data.getString(1),
                    data.getString(2),
                    data.getString(3),
                    data.getString(4),
                    data.getString(5),
                    data.getString(6),
                    data.getString(7),
                    data.getString(8),
                    data.getString(9),
                    data.getString(10),
                    data.getString(11)});
            }
        } catch (Exception e) {
            System.out.println("error " + e.getMessage());
        }
        t_kirim_duri.setModel(model);
    }

    public void tbkirimperawang() {
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("no");
        model.addColumn("Id Barang");
        model.addColumn("nama");
        model.addColumn("pengirim");
        model.addColumn("alamat");
        model.addColumn("tujuan");
        model.addColumn("berat");
        model.addColumn("biaya");
        model.addColumn("status");
        model.addColumn("tgl krim");
        model.addColumn("tgl estimasi");

        try {
            String sql = "SELECT * FROM tbkirim_perawang";
            java.sql.Connection con = (Connection) connect.configDB();
            java.sql.Statement sts = con.createStatement();
            java.sql.ResultSet data = sts.executeQuery(sql);
            while (data.next()) {
                model.addRow(new Object[]{
                    data.getString(1),
                    data.getString(2),
                    data.getString(3),
                    data.getString(4),
                    data.getString(5),
                    data.getString(6),
                    data.getString(7),
                    data.getString(8),
                    data.getString(9),
                    data.getString(10),
                    data.getString(11)});
            }
        } catch (Exception e) {
            System.out.println("error " + e.getMessage());
        }
        t_kirim_perawang.setModel(model);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        t_kirim_duri = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        t_kirim_perawang = new javax.swing.JTable();
        insert = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jumlah_kirimall = new javax.swing.JLabel();
        jmlduri = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jmlperawang = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        user = new javax.swing.JLabel();
        nama_t = new javax.swing.JLabel();
        kota = new javax.swing.JLabel();
        cetak = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1750, 990));
        getContentPane().setLayout(null);

        jTabbedPane1.setBackground(new java.awt.Color(51, 0, 255));

        jPanel1.setBackground(new java.awt.Color(153, 153, 255));
        jPanel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        t_kirim_duri.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        t_kirim_duri.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        t_kirim_duri.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                t_kirim_duriMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(t_kirim_duri);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1519, Short.MAX_VALUE)
                .addGap(24, 24, 24))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 488, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Table kirim duri", jPanel1);

        jPanel2.setBackground(new java.awt.Color(153, 153, 255));

        t_kirim_perawang.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        t_kirim_perawang.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        t_kirim_perawang.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                t_kirim_perawangMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(t_kirim_perawang);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 1518, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(25, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 483, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(25, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Table kirim parawang", jPanel2);

        getContentPane().add(jTabbedPane1);
        jTabbedPane1.setBounds(70, 310, 1560, 570);
        jTabbedPane1.getAccessibleContext().setAccessibleName("Table kiri8m duri");

        insert.setBackground(new java.awt.Color(153, 153, 255));
        insert.setEditable(true);
        insert.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        insert.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Kirim barang", "Duri", "Perawang" }));
        insert.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                insertMouseClicked(evt);
            }
        });
        insert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                insertActionPerformed(evt);
            }
        });
        insert.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                insertKeyTyped(evt);
            }
        });
        getContentPane().add(insert);
        insert.setBounds(1420, 60, 210, 70);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Jumlah barang di kirim");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(800, 10, 260, 70);

        jumlah_kirimall.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jumlah_kirimall.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jumlah_kirimall.setText("0");
        getContentPane().add(jumlah_kirimall);
        jumlah_kirimall.setBounds(740, 120, 110, 100);

        jmlduri.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jmlduri.setText("jLabel3");
        getContentPane().add(jmlduri);
        jmlduri.setBounds(1150, 170, 60, 30);

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Kirim perawang  :");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(980, 170, 170, 30);

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel4.setText("Kirim duri   :");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(1020, 210, 110, 16);

        jmlperawang.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jmlperawang.setText("jLabel4");
        getContentPane().add(jmlperawang);
        jmlperawang.setBounds(1150, 200, 60, 30);

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel5.setText("Total kirim");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(750, 100, 120, 29);

        user.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        user.setForeground(new java.awt.Color(255, 255, 255));
        user.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        user.setText("KOSONG");
        getContentPane().add(user);
        user.setBounds(240, 80, 220, 60);

        nama_t.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        nama_t.setForeground(new java.awt.Color(255, 255, 255));
        nama_t.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        nama_t.setText("KOSONG");
        getContentPane().add(nama_t);
        nama_t.setBounds(240, 120, 220, 80);

        kota.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        kota.setForeground(new java.awt.Color(255, 255, 255));
        kota.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        kota.setText("KOSONG");
        getContentPane().add(kota);
        kota.setBounds(240, 160, 220, 100);

        cetak.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        cetak.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Cetak table", "cab.Duri", "Cab.Perawang", " " }));
        cetak.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cetakActionPerformed(evt);
            }
        });
        getContentPane().add(cetak);
        cetak.setBounds(1420, 150, 210, 70);

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/view/application2.png"))); // NOI18N
        getContentPane().add(jLabel1);
        jLabel1.setBounds(0, -10, 1700, 980);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void insertMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_insertMouseClicked
        // TODO add your handling code here:

    }//GEN-LAST:event_insertMouseClicked

    private void insertKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_insertKeyTyped
        // TODO add your handling code here:

    }//GEN-LAST:event_insertKeyTyped

    private void insertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_insertActionPerformed
        // TODO add your handling code here:
        String asal = null;

        if (insert.getSelectedItem() == "Duri") {
            asal = "duri";
            System.out.println(asal);
            new insert_barang(user.getText(), nama_t.getText(), kota.getText(), asal).setVisible(true);
            this.setVisible(false);
        } else if (insert.getSelectedItem() == "Perawang") {
            asal = "perawang";
            new insert_barang(user.getText(), nama_t.getText(), kota.getText(), asal).setVisible(true);
            this.setVisible(false);
        }
    }//GEN-LAST:event_insertActionPerformed

    private void t_kirim_duriMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_t_kirim_duriMouseClicked
        try {
            // TODO add your handling code here:
            int row = t_kirim_duri.getSelectedRow();
            String no = (String) t_kirim_duri.getValueAt(row, 0);
            String id_barang = (String) t_kirim_duri.getValueAt(row, 1);
            String nama_barang = (String) t_kirim_duri.getValueAt(row, 2);
            String nama_pengirim = (String) t_kirim_duri.getValueAt(row, 3);
            String alamat_tujuan = (String) t_kirim_duri.getValueAt(row, 4);
            String nama_tujuan = (String) t_kirim_duri.getValueAt(row, 5);
            String berat = (String) t_kirim_duri.getValueAt(row, 6);
            String harga = (String) t_kirim_duri.getValueAt(row, 7);
            String status = (String) t_kirim_duri.getValueAt(row, 8);
            String tglkirim = (String) t_kirim_duri.getValueAt(row, 9);

            new edit_barang(no, id_barang, nama_barang, nama_pengirim, alamat_tujuan, nama_tujuan, berat, harga, status, "duri", user.getText(), nama_t.getText(), tglkirim).setVisible(true);

            this.setVisible(false);
        } catch (ParseException ex) {
            Logger.getLogger(application2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_t_kirim_duriMouseClicked

    private void t_kirim_perawangMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_t_kirim_perawangMouseClicked
        try {
            // TODO add your handling code here:
            int row = t_kirim_perawang.getSelectedRow();
            String no = (String) t_kirim_perawang.getValueAt(row, 0);
            String id_barang = (String) t_kirim_perawang.getValueAt(row, 1);
            String nama_barang = (String) t_kirim_perawang.getValueAt(row, 2);
            String nama_pengirim = (String) t_kirim_perawang.getValueAt(row, 3);
            String alamat_tujuan = (String) t_kirim_perawang.getValueAt(row, 4);
            String nama_tujuan = (String) t_kirim_perawang.getValueAt(row, 5);
            String berat = (String) t_kirim_perawang.getValueAt(row, 6);
            String harga = (String) t_kirim_perawang.getValueAt(row, 7);
            String status = (String) t_kirim_perawang.getValueAt(row, 8);
            String tglkirim = (String) t_kirim_perawang.getValueAt(row, 9);

            new edit_barang(no, id_barang, nama_barang, nama_pengirim, alamat_tujuan, nama_tujuan, berat, harga, status, "perawang", user.getText(), nama_t.getText(), tglkirim).setVisible(true);
            this.setVisible(false);
        } catch (ParseException ex) {
            Logger.getLogger(application2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_t_kirim_perawangMouseClicked

    private void cetakActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cetakActionPerformed
        // TODO add your handling code here:
        try {
            if ("cab.Duri" == cetak.getSelectedItem()) {
                InputStream is = application2.class.getResourceAsStream("/view/dataduri.jasper");
                JasperPrint jsprint = JasperFillManager.fillReport(is, null, connect.configDB());
                JasperViewer.viewReport(jsprint, false);
            } else if ("Cab.Perawang" == cetak.getSelectedItem()) {
                InputStream is = application2.class.getResourceAsStream("/view/perawang.jasper");
                JasperPrint jsprint = JasperFillManager.fillReport(is, null, connect.configDB());
                JasperViewer.viewReport(jsprint, false);
            }

        } catch (Exception e) {

        }

    }//GEN-LAST:event_cetakActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> cetak;
    private javax.swing.JComboBox<String> insert;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel jmlduri;
    private javax.swing.JLabel jmlperawang;
    private javax.swing.JLabel jumlah_kirimall;
    private javax.swing.JLabel kota;
    private javax.swing.JLabel nama_t;
    private javax.swing.JTable t_kirim_duri;
    private javax.swing.JTable t_kirim_perawang;
    private javax.swing.JLabel user;
    // End of variables declaration//GEN-END:variables
}
